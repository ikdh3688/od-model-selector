# od-model-selector

Dynamic model selection for object detection.


[Reference]

https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md

https://github.com/lbeaucourt/Object-detection
