import os
from utils.app_utils import *
import numpy as np
import tensorflow as tf
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util
import time

import logging
import logging.handlers
logging.basicConfig(format='%(message)s', level=logging.INFO)
logger = logging.getLogger("LOG")

file_handler = logging.FileHandler('log')
logger.addHandler(file_handler)


# Path to frozen detection graph. This is the actual model that is used for the object detection.
#PATH_TO_CKPT = 'model/frozen_inference_graph.pb'
PATH_TO_CKPT = os.getenv('PATH_TO_CKPT').split(':')[0]
PATH_TO_CKPTS = os.getenv('PATH_TO_CKPT').split(':')
#PATH_TO_CKPT = '/home/ikdh3688/Object-detection/model/faster_rcnn_inception_v2_coco_2018_01_28/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = 'model/mscoco_label_map.pbtxt'

PATH_TO_OUTPUT = os.getenv('PATH_TO_OUTPUT')

NUM_CLASSES = 90

prev_num_detections = []

# Loading label map
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                            use_display_name=True)
category_index = label_map_util.create_category_index(categories)

total_detections = []

def detect_objects(image_np, sess, detection_graph, num, output):
    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
    image_np_expanded = np.expand_dims(image_np, axis=0)
    #print(image_np_expanded.shape)
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    #print(image_tensor.shape)

    # Each box represents a part of the image where a particular object was detected.
    boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

    # Each score represent how level of confidence for each of the objects.
    # Score is shown on the result image, together with the class label.
    scores = detection_graph.get_tensor_by_name('detection_scores:0')
    
    classes = detection_graph.get_tensor_by_name('detection_classes:0')
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    # Actual detection.
    
    (boxes, scores, classes, num_detections) = sess.run(
        [boxes, scores, classes, num_detections],
        feed_dict={image_tensor: image_np_expanded})
#    print(boxes.shape)
#    print(scores)
#    print(classes)

    for i in range(100):
        if(scores[0][i] > 0):
#            print(num, boxes[0][i][0], boxes[0][i][1], boxes[0][i][2], boxes[0][i][3], scores[0][i], classes[0][i])
            output.write("{},{},{},{},{},{},{}\n".format(num, boxes[0][i][0], boxes[0][i][1],
                boxes[0][i][2], boxes[0][i][3], scores[0][i], classes[0][i]))
    global prev_num_detections
    prev_num_detections.append(num_detections[0])
    global total_detections
    total_detections.append(num_detections[0])
    if len(total_detections) > 20:
        total_detections.pop(0)


    logger.info(num_detections[0])
    #start_time = time.time()
    #(boxes, num_detections) = sess.run(
    #    [boxes, num_detections],
    #    feed_dict={image_tensor: image_np_expanded})
    #print("[%f] %f" % (time.time(),time.time()-start_time))
    #print(boxes)
    #print(num_detections)


    # Visualization of the results of a detection.
    vis_util.visualize_boxes_and_labels_on_image_array(
        image_np,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=4)

    return image_np

def get_graph_and_sess(sessions, graphs):
    # DONE (dohyung) : Re-design how to input thresholds.
    
    global prev_num_detections
    if len(prev_num_detections) == 0:
        return 0, graphs[0], sessions[0]
    # TODO (dohyung) : Reduce the computation cost formean and std.
    mean = np.mean(prev_num_detections)
    std = np.std(prev_num_detections)
    # Six sigma = 99.7% 
    bin_size = 2*3*std/len(sessions)
    thresholds = []
    for i in range(sessions):
        # The floor of thresholds is mean-3*sigma 
        thr = mean - 3*std + i*bin_size
        thresholds.append(thr)
    #thresholds = [22.59, 26.81,31.03]
    thresholds = thresholds[:len(sessions)]
    
    # The recent value is used for selection of a model in the next.
    prev_num_box = prev_num_detections[-1]
    for i, threshold in enumerate(thresholds):
        if prev_num_box < threshold:
            #logging.info("prev_num_box, threshold : %d %d" % (prev_num_box, threshold))
            return i, graphs[i], sessions[i]

    # Randomly Selection. 
    rand = np.random(len(sessions), 1)[0]
    #return len(graphs)-1, graphs[-1], sessions[-1]
    return rand, graphs[rand], sessions[rand]
SKIP_FRAME = 1
use_skip_frame = True
def worker(input_q, output_q):
    # Load a (frozen) Tensorflow model into memory.
    sessions = []
    graphs = [] 
    num_graphs = len(PATH_TO_CKPTS)
    for i in range(num_graphs):
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(PATH_TO_CKPTS[i], 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
            config = tf.ConfigProto()
            config.gpu_options.allow_growth = True 
            sess = tf.Session(graph=detection_graph, config=config)
            sessions.append(sess)
            graphs.append(detection_graph)
    
    output_dir = os.path.join(PATH_TO_OUTPUT, "output.txt")
    output = open(output_dir, "w")

    global total_detections, SKIP_FRAME
    fps = FPS().start()
    while True:
        if not use_skip_frame:
            SKIP_FRAME = 1

        while SKIP_FRAME > 0:
            fps.update()
            frame = input_q.get()
            SKIP_FRAME -= 1
            if SKIP_FRAME != 0:
                #output_q.put((frame[0], frame[1]))
                frame_rgb = cv2.cvtColor(frame[1], cv2.COLOR_BGR2RGB)
                output_q.put((frame[0], frame_rgb))

        # Check frame object is a 2-D array (video) or 1-D (webcam)
        if len(frame) == 2:
            frame_rgb = cv2.cvtColor(frame[1], cv2.COLOR_BGR2RGB)
            index, detection_graph, sess = get_graph_and_sess(sessions, graphs)
            SKIP_FRAME = index+1 # Drop Frames as much as index .
#            SKIP_FRAME = 3 
            output_q.put((frame[0], detect_objects(frame_rgb, sess, detection_graph, frame[0]+1,
                output)))
        else:
            frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            output_q.put(detect_objects(frame_rgb, sess, detection_graph))
    output.close()
    fps.stop()
    sess.close()
