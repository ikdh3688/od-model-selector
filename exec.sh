export PYTHONPATH=/home/ikdh3688/Object-detection/models/research:/home/ikdh3688/Object-detection/models/research/slim
#export INPUT_DIR=/home/ikdh3688/Object-detection/MOT17/train/MOT17-10-DPM
export INPUT_DIR=/home/ikdh3688/Object-detection/MOT17/train/MOT17-04-DPM

#export PATH_TO_CKPT=model/faster_rcnn_inception_v2_coco/frozen_inference_graph.pb
export PATH_TO_CKPT=model/ssdlite_mobilenet_v2_coco/frozen_inference_graph.pb
export PATH_TO_CKPT=$PATH_TO_CKPT:model/ssd_mobilenet_v1_fpn/frozen_inference_graph.pb
export PATH_TO_CKPT=$PATH_TO_CKPT:model/ssd_resnet50_v1_fpn/frozen_inference_graph.pb
#export PATH_TO_CKPT=model/ssd_resnet50_v1_fpn/frozen_inference_graph.pb
#export PATH_TO_CKPT=model/ssd_mobilenet_v1_fpn/frozen_inference_graph.pb
#export PATH_TO_CKPT=model/ssdlite_mobilenet_v2_coco/frozen_inference_graph.pb

export PATH_TO_OUTPUT=result/demo/
mkdir -p result/demo/
python3 my-object-detection.py -d 0 -o 0 -i test.mp4 -w 1 -q-size 20
