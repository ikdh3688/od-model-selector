#!/bin/bash

SCRIPT_ROOT=$(pwd)
RESULT_ROOT="${SCRIPT_ROOT}/result"

TIMESTAMP=`date +"%Y%m%d_%H%M%S"`

echo $TIMESTAMP

RESULT_DIR="${RESULT_ROOT}/result_${TIMESTAMP}"

MODEL_LIST="ssdlite_mobilenet_v2_coco ssd_mobilenet_v1_fpn ssd_resnet50_v1_fpn"

mkdir -p ${RESULT_DIR}
cp ${SCRIPT_ROOT}/${0} ${RESULT_DIR}/.

export PYTHONPATH=/home/ikdh3688/Object-detection/models/research:/home/ikdh3688/Object-detection/models/research/slim
export INPUT_DIR=/home/ikdh3688/Object-detection/MOT17/train/MOT17-02-DPM

export PATH_TO_CKPT=model/faster_rcnn_inception_v2_coco/frozen_inference_graph.pb

for model in ${MODEL_LIST}; do

	export PATH_TO_CKPT=model/${MODEL_LIST}/frozen_inference_graph.pb
	
	result_path=${RESULT_DIR}/${model}
	mkdir -p ${result_path}

	export PATH_TO_OUTPUT=${result_path}
	
	python3 my-object-detection.py -d 0 -o 0 -i test.mp4 -w 1 -q-size 20

done

